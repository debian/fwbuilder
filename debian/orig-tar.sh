#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
DIR=fwbuilder-$2
TAR=../fwbuilder_$2.orig.tar.gz

# clean up the upstream tarball
tar zxvf $3
# in the future, uncommun this line
#rm -rf $DIR*/src/antlr/
tar -c -z -f $TAR $DIR*
rm -rf $DIR* $3

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
    . .svn/deb-layout
    mv $TAR $origDir
    echo "moved $TAR to $origDir"
fi

exit 0
